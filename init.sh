#!/bin/sh

# User credentials
user=admin
email=admin@example.com
password=pass
file=db/db.sqlite3

if [ -z "$file" ]; then      # Если файла БД нет  
  # Развертываем структуру Sqlite3 
  python3 manage.py migrate  
  # Добавляем суперпользователя  в Django
  echo "from django.contrib.auth.models import User; User.objects.create_superuser('$user', '$email', '$password')" | python3 manage.py shell
else 
  # ТОлько развертываем структуру Sqlite3
  python3 manage.py migrate    
fi

